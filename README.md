# Migration Gite-Hurle Symfony4.x sous docker

## Docker

il m'a fallu un mois pour arriver à faire un environnement de travail entre une image postgres et une image php7 avec apache
me reste un problème de ServerName il faut que j'automatise avec un dockerfile je pense.
J'ai aussi des soucis de redirection de port, mais bon j'avance un peu sur le développement.

## Doctrine

Première étape on va demander à doctrine de créer les entités à partir de la base de donnée.
Cette dernière à été fait à la main, depuis le terminal de l'image docker de postgres

première commande donné gentillement par un ami :
`php bin/console doctrine:mapping:import "App\Entity" annotation --path=src/Entity`

les entitées sont bien crées mais il n'y a pas les repository associé

https://symfony.com/doc/current/doctrine/reverse_engineering.html

C'est good il faut:
 - rajouter les annotations `@ORM\Entity(repositoryClass="App\Repository\AdministrateurRepository")`
 - et un petit coup de `php bin/console make:entity --regenerate`


## Controler
Regroupe nos controller avec le namespace App/Controller

`php bin/console make:controller`

On a besoin de use :

```php
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
```

voici la syntaxe pour renvoyer une vue

`return new Response($this->twig->render('administration/espaces.html.twig'));`

## Template
comporte nos vue en .html.twig
Permet de centraliser nos vues le nom de la vue doit être suffixé de .html.twig

## Configuration
.env : permet de modifier la configuration de la BDD

## Lancement du serveur
`php -S 127.0.0.1:8000 -t public`
ou
`php bin/console server:run`

## BDD
la bdd est représenté par le fichier *dump-ghdb-201911012330.backup* à la racine du projet



