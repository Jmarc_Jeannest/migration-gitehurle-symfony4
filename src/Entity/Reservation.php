<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservation
 *
 * @ORM\Table(name="reservation", indexes={@ORM\Index(name="IDX_42C84955209CBC80", columns={"id_espace"}), @ORM\Index(name="IDX_42C84955BEC135E8", columns={"id_formule"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ReservationRepository")
 */
class Reservation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="reservation_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_reservation", type="datetime", nullable=false)
     */
    private $dateReservation;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=60, nullable=false)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="debut_sejour", type="datetime", nullable=false)
     */
    private $debutSejour;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fin_sejour", type="datetime", nullable=false)
     */
    private $finSejour;

    /**
     * @var \Espace
     *
     * @ORM\ManyToOne(targetEntity="Espace")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_espace", referencedColumnName="id")
     * })
     */
    private $idEspace;

    /**
     * @var \Formule
     *
     * @ORM\ManyToOne(targetEntity="Formule")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_formule", referencedColumnName="id")
     * })
     */
    private $idFormule;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateReservation(): ?\DateTimeInterface
    {
        return $this->dateReservation;
    }

    public function setDateReservation(\DateTimeInterface $dateReservation): self
    {
        $this->dateReservation = $dateReservation;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDebutSejour(): ?\DateTimeInterface
    {
        return $this->debutSejour;
    }

    public function setDebutSejour(\DateTimeInterface $debutSejour): self
    {
        $this->debutSejour = $debutSejour;

        return $this;
    }

    public function getFinSejour(): ?\DateTimeInterface
    {
        return $this->finSejour;
    }

    public function setFinSejour(\DateTimeInterface $finSejour): self
    {
        $this->finSejour = $finSejour;

        return $this;
    }

    public function getIdEspace(): ?Espace
    {
        return $this->idEspace;
    }

    public function setIdEspace(?Espace $idEspace): self
    {
        $this->idEspace = $idEspace;

        return $this;
    }

    public function getIdFormule(): ?Formule
    {
        return $this->idFormule;
    }

    public function setIdFormule(?Formule $idFormule): self
    {
        $this->idFormule = $idFormule;

        return $this;
    }


}
