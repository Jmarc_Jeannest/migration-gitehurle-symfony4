<?php

namespace App\Controller;

use App\Entity\Espace;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class EspaceController extends AbstractController
{

    /**
     * Attibuts me permettants de stocker les espaces récupérer depuis le repository
     * Je pensais grâce à ça ne pas avoir à refaire une vue spécifique pour le rendu d'une liste
     * on d'un seul élement compris dans un tableau, mais le rendu ne fonctionne pas comme je le souhaite
     */

     private $espaces = [];


    /**
     * Méthode  pour l'affichage des espaces du gîte.
     * @Route("/espace", name="espace")
     */
    public function index()
    {

        //Récupération de touts les espaces
        $this->espaces = $this->getDoctrine()
            ->getRepository(Espace::class)
            ->findAll();

        //Permet de transformer la description en tableau me permettant un affichage sous forme de liste
        $this->descriptionToArray();

        return $this->render('espace/index.html.twig', [
            'espaces' => $this->espaces,
        ]);
    }

    /**
     * Méthode pour l'affichage d'un espace identifier par son id passé en paramètre
     * @Route("/espace/{id}", methods={"GET"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function showEspace(int $id) {
        $espace = $this->getDoctrine()
            ->getRepository(Espace::class)
            ->find($id);

        array_push($this->espaces, $espace);

        //Permet de transformer la description tableau
        $this->descriptionToArray();

        return $this->render('espace/index.html.twig', [
            'espaces' => $this->espaces,
        ]);
    }

    /**
     * Méthode permettant la transformation en tableau de l'attribut descreption des objets
     * Espace récupéré.
     */
    private function descriptionToArray() {

        //Permet de transformer la description en tableau me permettant un affichage sous forme de liste
        foreach ($this->espaces as $index=>$espace) {

            $items = explode("+", $espace->getDescription());
            $this->espaces[$index]->items = $items;
        }

    }
}
