<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class NavBarController extends AbstractController
{
    /**
     * @Route("/nav/bar", name="nav_bar")
     */
    public function index()
    {
        return $this->render('components/nav_bar/index.html.twig', [
            'controller_name' => 'NavBarController',
        ]);
    }
}
