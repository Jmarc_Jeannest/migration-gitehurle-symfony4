<?php

namespace App\Controller;

use App\Entity\Formule;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DomCrawler\Form;
use Symfony\Component\Routing\Annotation\Route;

class FormuleController extends AbstractController
{

    private $formules = [];
    /**
     * Route principale pour l'affichage des formules du gîtes
     * @Route("/formule", name="formule")
     */
    public function index()
    {
        $this->formules = $this->getDoctrine()
            ->getRepository(Formule::class)
            ->findAll();

        return $this->render('formule/index.html.twig', [
            'formules' => $this->formules,
        ]);
    }

    /**
     * Méthode permettant d'afficher une formule via son id passé en paramètre
     *
     * @Route ("/formule/{id}", methods={"GET"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showFormule(int $id) {
        $formule = $this->getDoctrine()
            ->getRepository(Formule::class)
            ->find($id);

        array_push($this->formules, $formule);

        return $this->render('formule/index.html.twig', [
            'formules' => $this->formules,
        ]);
    }
}
