<?php

namespace App\Controller;

use App\Entity\Espace;
use App\Entity\Formule;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\EspaceRepository;

class AccueilController extends AbstractController
{
    /**
     * @Route("/accueil", name="accueil")
     */
    public function index()
    {
        //Récupération de touts les espaces
        $espaces = $this->getDoctrine()
            ->getRepository(Espace::class)
            ->findAll();

        //Récupération de toutes les formules
        $formules = $this->getDoctrine()
            ->getRepository(Formule::class)
            ->findAll();

        //Renvoie de la vue avec les variables
        return $this->render('accueil/index.html.twig', [
            'espaces' => $espaces,
            'formules' => $formules
        ]);
    }
}
